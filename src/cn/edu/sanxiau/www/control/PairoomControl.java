package cn.edu.sanxiau.www.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.Exam;
import cn.edu.sanxiau.www.model.PaiRoom;
import cn.edu.sanxiau.www.model.RoomExam;
import frame.utils.control.BaseControl2;

public class PairoomControl extends BaseControl2 {
	
	public String list(HttpServletRequest req, HttpServletResponse resp) {

		List<PaiRoom> pairoomList_db_list = examService.queryPrByExamId();
		System.out.println(pairoomList_db_list.size());
		req.setAttribute("pairoomList_db_list", pairoomList_db_list);
		// 2、跳转页面
		return "view/system/pairoom/pairoomList.jsp";
	}

	public int getPC(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String value = req.getParameter("pc");
		if (value == null || value.trim().isEmpty()) {
			return 1;
		}
		return Integer.parseInt(value);

	}

	/*public String listStu(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// 3、调用业务

		List<PaiRoom> pairoomList_db_list = examService.queryPrByExamId();

		req.setAttribute("pairoomList_db_list", pairoomList_db_list);
		// 2、跳转页面
		return "view/system/pairoom/pakeListStu.jsp";
	}*/

	public String addUI(HttpServletRequest req, HttpServletResponse resp)  {

		List<PaiRoom> classRoom_db_list = classRoomService.queryAllExam();
		List<Exam> exam_db_list = examService.queryAllExamByState("0");
		req.setAttribute("classRoom_db_list", classRoom_db_list);
		req.setAttribute("exam_db_list", exam_db_list);
		return "view/system/pairoom/addPaiRoom.jsp";
	}

	public String queryEmputyRoom(HttpServletRequest req, HttpServletResponse resp){
	
		String examIdStr = req.getParameter("examIdStr");
		String time = req.getParameter("time");
		String date = req.getParameter("date");
		int examId = Integer.parseInt(examIdStr);
		
		req.getSession().setAttribute("examId", examId);
		req.getSession().setAttribute("time", time);
		req.getSession().setAttribute("date", date);
		List<PaiRoom> classRoom_list_em = new ArrayList<PaiRoom>();
		
		List<PaiRoom> classRoom_all_list = classRoomService.queryAllClassRoom2();
		for (PaiRoom pairoom_all : classRoom_all_list) {
			//System.out.println(pairoom_all.getRdate());
			if(pairoom_all.getRdate().equals("0"))
			{
				classRoom_list_em.add(pairoom_all);
			}else{
				
				List<PaiRoom> roomexam_db_list = classRoomService.queryAllRoomByRoomId2(pairoom_all.getRoomId());
				for (PaiRoom pairoom : roomexam_db_list) {
					if(!pairoom.getTime().equals(time)||!pairoom.getDate().equals(date)){
						classRoom_list_em.add(pairoom);
					}
			}
		}
		}
		req.setAttribute("classRoom_list_em", classRoom_list_em);
		return "view/system/pairoom/addPaiRoom2.jsp";
		/*req.getSession().setAttribute("examId", examId);
		req.getSession().setAttribute("time", time);
		req.getSession().setAttribute("date", date);
		//System.out.println("time"+time+"-----date"+date);
		List<paiRoom> classRoom_list_em = new ArrayList<paiRoom>();
		List<paiRoom> classRoom_all_list = classRoomService.queryAllRoom();
		for (pairoom classRoom : classRoom_all_list) {
			if (!classRoom.getState().equals(time) || !classRoom.getDate().equals(date))
				{//System.out.println("state:"+classRoom.getState()+"----date:"+classRoom.getDate());
				classRoom_list_em.add(classRoom);}
		}
		req.setAttribute("classRoom_list_em", classRoom_list_em);
		return "view/system/pairoom/addpairoom2.jsp";*/
		
	}

	public String add(HttpServletRequest req, HttpServletResponse resp) {

		int examId = (int) req.getSession().getAttribute("examId");
		String time = (String) req.getSession().getAttribute("time");
		String date = (String) req.getSession().getAttribute("date");
		String roomIdStr = req.getParameter("roomIdStr");
		int roomId = Integer.parseInt(roomIdStr);

		RoomExam roomExam = new RoomExam();
		roomExam.setExamId(examId);
		roomExam.setRoomId(roomId);
		roomExam.setTime(time);
		roomExam.setDate(date);

		int n = roomExamService.addRoomExamByRoomExam(roomExam);

		List<PaiRoom> pairoomList_db_list = examService.queryPrByExamId();

		req.setAttribute("pairoomList_db_list", pairoomList_db_list);
		// 2、跳转页面
		return "view/system/pairoom/pairoomList.jsp";
	}

	/*public String delete(HttpServletRequest req, HttpServletResponse resp) {
		String examIdStr = req.getParameter("examIdStr");
		String roomexamIdStr = req.getParameter("roomexamIdStr");
		String roomIdStr = req.getParameter("roomIdStr");

		int examId = Integer.parseInt(examIdStr);
		int roomexamId = Integer.parseInt(roomexamIdStr);
		int roomId = Integer.parseInt(roomIdStr);

		int n = examService.updateExamStateByExamId(examId);
		int m = classRoomService.updateRoomStateByRoomId(roomId);
		int i = roomexamService.deleteRoomExamByRoomExamId(roomexamId);

		List<PaiRoom> pairoomList_db_list = examService.queryPrByExamId();

		req.setAttribute("pairoomList_db_list", pairoomList_db_list);
		// 2、跳转页面
		return "view/system/pairoom/pakeList.jsp";
	}

	public String updateUI(HttpServletRequest req, HttpServletResponse resp) {
		String examIdStr = req.getParameter("examIdStr");
		int examId = Integer.parseInt(examIdStr);

		List<PaiRoom> pairoom_db_list = examService.queryPrByExamId(examId);
		req.setAttribute("pairoom_db_list", pairoom_db_list);

		return "view/system/pairoom/updatepairoom.jsp";
	}

	public String queryEmputyRoom1(HttpServletRequest req, HttpServletResponse resp) {

		String examIdStr = req.getParameter("examIdStr");
		String time = req.getParameter("time");
		String date = req.getParameter("date");
		String preroomIdStr = req.getParameter("preroomIdStr");
		String roomexamId = req.getParameter("roomexamId");
		int examId = Integer.parseInt(examIdStr);

		req.getSession().setAttribute("examId1", examId);
		req.getSession().setAttribute("time1", time);
		req.getSession().setAttribute("date1", date);
		req.getSession().setAttribute("preroomId1", Integer.parseInt(preroomIdStr));
		// req.getSession().setAttribute("roomexamId1", roomexamId);
		System.out.println("time" + time + "-----date" + date);
		List<PaiRoom> classRoom_list_em = new ArrayList<PaiRoom>();
		
		List<PaiRoom> classRoom_all_list = classRoomService.queryAllClassRoom();
		for (PaiRoom pairoom_all : classRoom_all_list) {
			
			if(pairoom_all.getRdate().equals("0"))
			{
				System.out.println(pairoom_all.getRdate()+"---name"+pairoom_all.getRoomName());
				classRoom_list_em.add(pairoom_all);
			}else if(!pairoom_all.getRdate().equals("0")){
				
				List<PaiRoom> roomexam_db_list = classRoomService.queryAllRoomByRoomId(pairoom_all.getRoomId());
				for (PaiRoom pairoom : roomexam_db_list) {
					if((!pairoom.getTime().equals(time)||!pairoom.getDate().equals(date))&&pairoom.getexamId()!=examId){
						classRoom_list_em.add(pairoom);
					}
			}
		}
		}
		req.setAttribute("classRoom_list_em", classRoom_list_em);
		return "view/system/pairoom/updatepairoom2.jsp";

	}

	public String update(HttpServletRequest req, HttpServletResponse resp) {

		String roomIdStr = req.getParameter("roomIdStr");
		int roomId = Integer.parseInt(roomIdStr);
		int examId = (int) req.getSession().getAttribute("examId1");
		String time = (String) req.getSession().getAttribute("time1");
		String date = (String) req.getSession().getAttribute("date1");
		int preroomId = (int) req.getSession().getAttribute("preroomId1");
		// int roomexamId = (int)
		// req.getSession().getAttribute("roomexamId1");

		PaiRoom pairoom = new PaiRoom();
		pairoom.setexamId(examId);
		pairoom.setRoomId(roomId);
		// pairoom.setRoomexamId(roomexamId);
		pairoom.setTime(time);
		pairoom.setDate(date);
		pairoom.setPreroomId(preroomId);

		int n = examService.updateExamByPairoom(pairoom);

		List<PaiRoom> pairoomList_db_list = examService.queryPrByExamId();

		req.setAttribute("pairoomList_db_list", pairoomList_db_list);
		// 2、跳转页面
		return "view/system/pairoom/pakeList.jsp";

	}*/

}
