package cn.edu.sanxiau.www.control;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.Exam;
import frame.utils.control.BaseControl2;
import frame.utils.model.PageBean;

public class ExamControl extends BaseControl2{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9140233584632967003L;
	public int getPC(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String value = req.getParameter("pc");
		if (value == null || value.trim().isEmpty()) {
			return 1;
		}
		return Integer.parseInt(value);

	}
	
	// 1.查询
		public String list(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
			// 1、接受数据
			// a、 获取页面传递的pc:当前页码（page code）
			int pc = getPC(req, resp);
			// b、给定ps的值:每页记录数（page size）
			int ps = 6;

			// 3、调用业务
			PageBean<Exam> pageBean = examService.queryAllPage(pc, ps);
			req.setAttribute("pb", pageBean);
			// 3、调用业务
			List<Exam> exam_db_list = examService.queryAllExam();
//			for (User user : user_db_list) {
//				System.out.println("----------list()-----------" + user);
//			}
			// 返回数据到页面
			req.setAttribute("exam_db_list", exam_db_list);

			// 2、跳转页面
			return "view/system/exam/examList.jsp";

		}

		// 2.增加
		public String add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
			
			// a、 获取页面传递的pc:当前页码（page code）
						int pc = getPC(req, resp);
						// b、给定ps的值:每页记录数（page size）
						int ps = 6;

						// 3、调用业务
						PageBean<Exam> pageBean = examService.queryAllPage(pc, ps);
						req.setAttribute("pb", pageBean);
			
			// 1、接受数据
			String examName = req.getParameter("examName");
			String examType = req.getParameter("examType");
			String examState = req.getParameter("examState");

			// 装载
			Exam exam = new Exam();
			exam.setExamName(examName);
			exam.setExamType(examType);
			exam.setExamState(examState);
			 

//			System.out.println("-----------add()----------" + user);

			// 3、调用业务
			int n = examService.addExamByExam(exam);

			// 返回数据到页面
			List<Exam> exam_db_list = examService.queryAllExam();
			req.setAttribute("exam_db_list", exam_db_list);
			// 2、跳转页面
			resp.sendRedirect("http://localhost:8080/XGYQSystem13/examControl.do?method=list");
			return "";

		}

		// 3.删除
		public String delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

			// a、 获取页面传递的pc:当前页码（page code）
						int pc = getPC(req, resp);
						// b、给定ps的值:每页记录数（page size）
						int ps = 6;

						// 3、调用业务
						PageBean<Exam> pageBean = examService.queryAllPage(pc, ps);
						req.setAttribute("pb", pageBean);
			
			
			// 1、接受数据
			String examIdStr = req.getParameter("examIdStr");
//			System.out.println("-----------delete()----------" + userIdStr);
			// 字符类型转成数字类型
			int examId = Integer.parseInt(examIdStr);

			// 3、调用业务
			int m = examService.deleteExamByExamId(examId);

			// 返回数据到页面
			List<Exam> exam_db_list = examService.queryAllExam();
			req.setAttribute("exam_db_list", exam_db_list);
			// 2、跳转页面
			resp.sendRedirect("http://localhost:8080/XGYQSystem13/examControl.do?method=list");
			return "";

		}

		// 4.修改ui
		public String updateUI(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

			// a、 获取页面传递的pc:当前页码（page code）
						int pc = getPC(req, resp);
						// b、给定ps的值:每页记录数（page size）
						int ps = 6;

						// 3、调用业务
						PageBean<Exam> pageBean = examService.queryAllPage(pc, ps);
						req.setAttribute("pb", pageBean);
			
			
			// 1、接受数据
			String examIdStr = req.getParameter("examIdStr");
			// 字符转成数字类型
			int examId = Integer.parseInt(examIdStr);
			// 3、调用业务
			Exam exam_db = examService.queryExamByExamId(examId);
//			System.out.println("-------------update()--------" + exam_db);

			// 返回数据到页面
			req.setAttribute("exam_db", exam_db);

			// 2、跳转页面
			return "view/system/exam/updateExams.jsp";
		}

		// 4.修改
		public String update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

			// a、 获取页面传递的pc:当前页码（page code）
						int pc = getPC(req, resp);
						// b、给定ps的值:每页记录数（page size）
						int ps = 6;

						// 3、调用业务
						PageBean<Exam> pageBean = examService.queryAllPage(pc, ps);
						req.setAttribute("pb", pageBean);
			
			
			// 1、接受数据
			String examIdStr = req.getParameter("examIdStr");
			int examId = Integer.parseInt(examIdStr);
			String examName = req.getParameter("examName");
			String examType = req.getParameter("examType");
			String examState = req.getParameter("examState");
			 

			// 实例化
			Exam  exam = new Exam();
			exam.setExamId(examId);
			exam.setExamName(examName);
			exam.setExamType(examType);
			exam.setExamState(examState);
			
			 
//			System.out.println("-------------update()--------" + user);
			// 3、调用业务
			int n = examService.updateExamByExam(exam);

			// 2、跳转页面
			// 返回数据到页面
			List<Exam> exam_db_list = examService.queryAllExam();
			req.setAttribute("exam_db_list", exam_db_list);
			// 2、跳转页面
			resp.sendRedirect("http://localhost:8080/XGYQSystem13/examControl.do?method=list");
			return "";

		}

	 
}
