package cn.edu.sanxiau.www.control;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.Course;
import cn.edu.sanxiau.www.service.CourseService;
import frame.utils.control.BaseControl2;
import frame.utils.model.PageBean;

public class CourseControl extends BaseControl2 {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2038948364732300857L;

	// 1.查询
	public String list(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 1、接受数据
		// a、 获取页面传递的pc:当前页码（page code）
		int pc = getPC(req, resp);
		// b、给定ps的值:每页记录数（page size）
		int ps = 6;

		// 3、调用业务
		PageBean<Course> pageBean = courseService.queryAllPage(pc, ps);
		req.setAttribute("pb", pageBean);
				
		List<Course> course_db_list = courseService.queryAllCourse();
		// for (course course : course_db_list) {
		// System.out.println("----------list()-----------" + course);
		// }
		// 返回数据到页面
		req.setAttribute("course_db_list", course_db_list);

		// 2、跳转页面
		return "/view/system/course/courseList.jsp";

	}

	// 2.增加
	public String add(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		
		
		// a、 获取页面传递的pc:当前页码（page code）
				int pc = getPC(req, resp);
				// b、给定ps的值:每页记录数（page size）
				int ps = 6;

				// 3、调用业务
				PageBean<Course> pageBean = courseService.queryAllPage(pc, ps);
				req.setAttribute("pb", pageBean);
		
		
		// 1、接受数据
		String courseName = req.getParameter("courseName");
		String courseTeacher = req.getParameter("courseTeacher");
		String score = req.getParameter("score");

		// 装载
		Course course = new Course();
		course.setCourseName(courseName);
		course.setCourseTeacher(courseTeacher);
		course.setScore(score);

		// System.out.println("-----------add()----------" + course);

		// 3、调用业务
		int n = courseService.addCourseByCourse(course);

		// 返回数据到页面
		List<Course> course_db_list = courseService.queryAllCourse();
		req.setAttribute("course_db_list", course_db_list);
		// 2、跳转页面
		resp.sendRedirect("http://localhost:8080/XGYQSystem13/courseControl.do?method=list");
		return "";

	}

	// 3.删除
	public String delete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

		// a、 获取页面传递的pc:当前页码（page code）
				int pc = getPC(req, resp);
				// b、给定ps的值:每页记录数（page size）
				int ps = 6;

				// 3、调用业务
				PageBean<Course> pageBean = courseService.queryAllPage(pc, ps);
				req.setAttribute("pb", pageBean);
		
		// 1、接受数据
		String courseIdStr = req.getParameter("courseIdStr");
		// System.out.println("-----------delete()----------" + courseIdStr);
		// 字符类型转成数字类型
		int courseId = Integer.parseInt(courseIdStr);

		// 3、调用业务
		int m = courseService.deleteCourseByCourseId(courseId);

		// 返回数据到页面
		List<Course> course_db_list = courseService.queryAllCourse();
		req.setAttribute("course_db_list", course_db_list);
		// 2、跳转页面
		resp.sendRedirect("http://localhost:8080/XGYQSystem13/courseControl.do?method=list");
		return "";

	}

	// 4.修改ui
	public String updateUI(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

		// a、 获取页面传递的pc:当前页码（page code）
				int pc = getPC(req, resp);
				// b、给定ps的值:每页记录数（page size）
				int ps = 6;

				// 3、调用业务
				PageBean<Course> pageBean = courseService.queryAllPage(pc, ps);
				req.setAttribute("pb", pageBean);
		
		
		// 1、接受数据
		String courseIdStr = req.getParameter("courseIdStr");
		// 字符转成数字类型
		int courseId = Integer.parseInt(courseIdStr);
		// 3、调用业务
		Course course_db = courseService.queryCourseByCourseId(courseId);
		// System.out.println("-------------update()--------" + course_db);

		// 返回数据到页面
		req.setAttribute("course_db", course_db);

		// 2、跳转页面
		return "view/system/course/updateCourse.jsp";
	}

	// 4.修改
	public String update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{

		/*// a、 获取页面传递的pc:当前页码（page code）
				int pc = getPC(req, resp);
				// b、给定ps的值:每页记录数（page size）
				int ps = 6;

				// 3、调用业务
				PageBean<Course> pageBean = courseService.queryAllPage(pc, ps);
				req.setAttribute("pb", pageBean);
		*/
		// 1、接受数据
		String courseIdStr = req.getParameter("courseIdStr");
		int courseId = Integer.parseInt(courseIdStr);
		String courseName = req.getParameter("courseName");
		String courseTeacher = req.getParameter("courseTeacher");
		String score = req.getParameter("score");
	
		Course course = new Course();
		course.setCourseId(courseId);
		course.setCourseName(courseName);
		course.setCourseTeacher(courseTeacher);
		course.setScore(score);

		 System.out.println("-------------update()--------" + course);
		// 3、调用业务
		int n = courseService.updateCourseByCourse(course);

		// 返回数据到页面
		List<Course> course_db_list = courseService.queryAllCourse();
		req.setAttribute("course_db_list", course_db_list);
		
		
	/*	PageBean<Course> pageBean = CourseService.queryAllPage(pc, ps);
		req.setAttribute("pb", pageBean);*/
		
		// 2、跳转页面
		resp.sendRedirect("http://localhost:8080/XGYQSystem13/courseControl.do?method=list");
		return "";
		
  }
	
	public int getPC(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String value = req.getParameter("pc");
		if (value == null || value.trim().isEmpty()) {
			return 1;
		}
		return Integer.parseInt(value);

	}

	/*public String listPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 1、接受数据
		// a、 获取页面传递的pc:当前页码（page code）
		int pc = getPC(req, resp);
		// b、给定ps的值:每页记录数（page size）
		int ps = 6;

		// 3、调用业务
		PageBean<Course> pageBean = courseService.queryAllPage(pc, ps);
		req.setAttribute("pb", pageBean);

		// 2、跳转页面
		return "view/system/course/courseList.jsp";
	}*/

	/*// 5.分配角色UI
	public String FPcourseUI(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String userIdStr = req.getParameter("userIdStr");
		int userId = Integer.parseInt(userIdStr);
		String name = req.getParameter("name");
		// System.out.println("----------------------" + userId);

		// 3、调用业务
		// a.用户数据
		User user_db = userService.queryUserByUserId(userId);
		req.setAttribute("user_db", user_db);

		// b.角色列表数据
		List<course> course_db_list = courseService.queryAllcourse();
		req.setAttribute("course_db_list", course_db_list);
		
		
		// d.选中数据
		course course_XZ = courseService.querycourseByUserId(userId);
		req.setAttribute("course_XZ", course_XZ);
		System.out.println("------course_XZ---------" + course_XZ);


		// 2、跳转页面
		return "view/system/course/FPcourseUI.jsp";

	}

	// 6.分配角色
	public String FPcourse(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String userIdStr = req.getParameter("userIdStr");
		String courseIdStr = req.getParameter("courseIdStr");

		// 字符类型转 int类型
		int userId = Integer.parseInt(userIdStr);
		int courseId = Integer.parseInt(courseIdStr);

//		System.out.println("------userId---------" + userId);
//		System.out.println("--------courseId-------" + courseId);
		// 3、调用业务
		// 封装思想
		Usercourse usercourse = new Usercourse();
		usercourse.setUserId(userId);
		usercourse.setcourseId(courseId);
		// 增加时候先删除数据库
		int m = usercourseService.deleteUsercourseByUserId(userId);
		// 增加业务
		int n = usercourseService.addUsercourseByUsercourse(usercourse);

		// 2、跳转页面
		// a.用户数据
		User user_db = userService.queryUserByUserId(userId);
		req.setAttribute("user_db", user_db);

		// b.角色列表数据
		List<course> course_db_list = courseService.queryAllcourse();
		req.setAttribute("course_db_list", course_db_list);

		// c.角色列表数据
		req.setAttribute("message", "角色分配成功！重新分配吗？");

		// d.选中数据
		course course_XZ = courseService.querycourseByUserId(userId);
		req.setAttribute("course_XZ", course_XZ);
		System.out.println("------course_XZ---------" + course_XZ);

		// 2、跳转页面
		return "view/system/course/FPcourseUI.jsp";

	}*/

}
