package cn.edu.sanxiau.www.control;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.edu.sanxiau.www.model.Role;
import cn.edu.sanxiau.www.model.User;
import cn.edu.sanxiau.www.model.UserRole;
import frame.utils.control.BaseControl2;

public class RoleControl extends BaseControl2 {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2038948364732300857L;

	// 1.查询
	public String list(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据

		// 3、调用业务
		List<Role> role_db_list = roleService.queryAllRole();
		// for (Role role : role_db_list) {
		// System.out.println("----------list()-----------" + role);
		// }
		// 返回数据到页面
		req.setAttribute("role_db_list", role_db_list);

		// 2、跳转页面
		return "view/system/role/roleList.jsp";

	}

	// 2.增加
	public String add(HttpServletRequest req, HttpServletResponse resp) {
		// 1、接受数据
		String name = req.getParameter("name");
		String state = req.getParameter("state");

		// 装载
		Role role = new Role();
		role.setName(name);
		role.setState(state);

		// System.out.println("-----------add()----------" + role);

		// 3、调用业务
		int n = roleService.addRoleByRole(role);

		// 返回数据到页面
		List<Role> role_db_list = roleService.queryAllRole();
		req.setAttribute("role_db_list", role_db_list);
		// 2、跳转页面
		return "view/system/role/roleList.jsp";

	}

	// 3.删除
	public String delete(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String roleIdStr = req.getParameter("roleIdStr");
		// System.out.println("-----------delete()----------" + roleIdStr);
		// 字符类型转成数字类型
		int roleId = Integer.parseInt(roleIdStr);

		// 3、调用业务
		int m = roleService.deleteRoleByRoleId(roleId);

		// 返回数据到页面
		List<Role> role_db_list = roleService.queryAllRole();
		req.setAttribute("role_db_list", role_db_list);
		// 2、跳转页面
		return "view/system/role/roleList.jsp";

	}

	// 4.修改ui
	public String updateUI(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String roleIdStr = req.getParameter("roleIdStr");
		// 字符转成数字类型
		int roleId = Integer.parseInt(roleIdStr);
		// 3、调用业务
		Role role_db = roleService.queryRoleByRoleId(roleId);
		// System.out.println("-------------update()--------" + role_db);

		// 返回数据到页面
		req.setAttribute("role_db", role_db);

		// 2、跳转页面
		return "view/system/role/updateRole.jsp";
	}

	// 4.修改
	public String update(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String roleIdStr = req.getParameter("roleIdStr");
		int roleId = Integer.parseInt(roleIdStr);
		String name = req.getParameter("name");
		String state = req.getParameter("state");

		// 实例化
		Role role = new Role();
		role.setRoleId(roleId);
		role.setName(name);
		role.setState(state);

		// System.out.println("-------------update()--------" + role);
		// 3、调用业务
		int n = roleService.updateRoleByRoler(role);

		// 返回数据到页面
		List<Role> role_db_list = roleService.queryAllRole();
		req.setAttribute("role_db_list", role_db_list);
		// 2、跳转页面
		return "view/system/role/roleList.jsp";

	}

	// 5.分配角色UI
	public String FPRoleUI(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String userIdStr = req.getParameter("userIdStr");
		int userId = Integer.parseInt(userIdStr);
		String name = req.getParameter("name");
		// System.out.println("----------------------" + userId);

		// 3、调用业务
		// a.用户数据
		User user_db = userService.queryUserByUserId(userId);
		req.setAttribute("user_db", user_db);

		// b.角色列表数据
		List<Role> role_db_list = roleService.queryAllRole();
		req.setAttribute("role_db_list", role_db_list);
		
		
		// d.选中数据
		Role role_XZ = roleService.queryRoleByUserId(userId);
		req.setAttribute("role_XZ", role_XZ);
		System.out.println("------role_XZ---------" + role_XZ);


		// 2、跳转页面
		return "view/system/role/FPRoleUI.jsp";

	}

	// 6.分配角色
	public String FPRole(HttpServletRequest req, HttpServletResponse resp) {

		// 1、接受数据
		String userIdStr = req.getParameter("userIdStr");
		String roleIdStr = req.getParameter("roleIdStr");

		// 字符类型转 int类型
		int userId = Integer.parseInt(userIdStr);
		int roleId = Integer.parseInt(roleIdStr);

//		System.out.println("------userId---------" + userId);
//		System.out.println("--------roleId-------" + roleId);
		// 3、调用业务
		// 封装思想
		UserRole userRole = new UserRole();
		userRole.setUserId(userId);
		userRole.setRoleId(roleId);
		// 增加时候先删除数据库
		int m = userRoleService.deleteUserRoleByUserId(userId);
		// 增加业务
		int n = userRoleService.addUserRoleByUserRole(userRole);

		// 2、跳转页面
		// a.用户数据
		User user_db = userService.queryUserByUserId(userId);
		req.setAttribute("user_db", user_db);

		// b.角色列表数据
		List<Role> role_db_list = roleService.queryAllRole();
		req.setAttribute("role_db_list", role_db_list);

		// c.角色列表数据
		req.setAttribute("message", "角色分配成功！重新分配吗？");

		// d.选中数据
		Role role_XZ = roleService.queryRoleByUserId(userId);
		req.setAttribute("role_XZ", role_XZ);
		System.out.println("------role_XZ---------" + role_XZ);

		// 2、跳转页面
		return "view/system/role/FPRoleUI.jsp";

	}

}
