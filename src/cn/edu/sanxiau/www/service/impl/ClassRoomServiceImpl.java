package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.ClassRoomDao;
import cn.edu.sanxiau.www.dao.impl.ClassRoomDaoImpl;
import cn.edu.sanxiau.www.model.ClassRoom;
import cn.edu.sanxiau.www.model.PaiKe;
import cn.edu.sanxiau.www.model.PaiRoom;
import cn.edu.sanxiau.www.model.RoomCourse;
import cn.edu.sanxiau.www.model.User2;
import cn.edu.sanxiau.www.service.ClassRoomService;
import frame.utils.model.PageBean;

public class ClassRoomServiceImpl implements ClassRoomService {
	
	ClassRoomDao classRoomDao = new ClassRoomDaoImpl();

	@Override
	public List<ClassRoom> queryAllClassroom() {
		return classRoomDao.queryAllClassroom();
	}

	@Override
	public int addClassRoomByClassRoomr(ClassRoom classroom) {
		return classRoomDao.addClassRoomByClassRoomr(classroom);
	}

	@Override
	public int deleteClassRoomByClassRoomId(int roomId) {
		return classRoomDao.deleteClassRoomByClassRoomId(roomId);
	}

	@Override
	public ClassRoom queryClassRoomByClassRoomId(int roomId) {
		return classRoomDao.queryClassRoomByClassRoomId(roomId);
	}

	@Override
	public int updateClassRoomByClassRoom(ClassRoom classroom) {
		return classRoomDao.updateClassRoomByClassRoom(classroom);
	}

	@Override
	public PageBean<ClassRoom> queryAllPage(int pc, int ps) {
		// 实例化PageBean
		PageBean<ClassRoom> pb = new PageBean<ClassRoom>();
		pb.setPc(pc);
		pb.setPs(ps);

		// 查询总记录数:总记录数（total record）
		int tr = classRoomDao.queryClassRoomTotalRecord();
		pb.setTr(tr);

		// 查询当前页的记录
		List<ClassRoom> beanList = classRoomDao.queryCurrentPageDataList((pc - 1) * ps, ps);
		pb.setBeanList(beanList);
		return pb;
	}
	
	@Override
	public List<ClassRoom> queryClassRoomByCourseId(int courseId) {
		// TODO Auto-generated method stub
		return classRoomDao.queryClassRoomByCourseId(courseId);
	}


	@Override
	public List<PaiKe> queryAllCourse() {    ///addUI
		// TODO Auto-generated method stub
		return classRoomDao.queryAllCourse();
	}


	@Override
	public String queryRoomNameByCourseId(int courseId) {
		// TODO Auto-generated method stub
		return classRoomDao.queryRoomNameByCourseId(courseId);
	}


	@Override
	public int updateRoomStateByRoomId(int roomId) {
		// TODO Auto-generated method stub
		return classRoomDao.updateRoomStateByRoomId(roomId);
	}


	@Override
	public List<PaiKe> queryAllRoomByRoomId(int roomId) {   ////queryEmputyRoom
		// TODO Auto-generated method stub
		return classRoomDao.queryAllRoomByRoomId(roomId);
	}


	@Override
	public List<PaiKe> queryAllClassRoom() {
		// TODO Auto-generated method stub
		return classRoomDao.queryAllClassRoom();
	}

	@Override
	public List<PaiRoom> queryAllExam() {         //addUI
		// TODO Auto-generated method stub
		return classRoomDao.queryAllExam();
	}

	@Override
	public List<PaiRoom> queryAllClassRoom2() {       //queryAllRoomByRoomId2(int roomId)
		// TODO Auto-generated method stub
		return classRoomDao.queryAllClassRoom2();
	}

	@Override
	public List<PaiRoom> queryAllRoomByRoomId2(int roomId) {    //queryAllRoomByRoomId2(int roomId)
		// TODO Auto-generated method stub
		return classRoomDao.queryAllRoomByRoomId2(roomId);
	}

	
}
