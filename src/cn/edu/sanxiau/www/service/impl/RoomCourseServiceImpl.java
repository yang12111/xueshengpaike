package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.RoomCourseDao;
import cn.edu.sanxiau.www.dao.impl.RoomCourseDaoImpl;
import cn.edu.sanxiau.www.model.RoomCourse;
import cn.edu.sanxiau.www.service.RoomCourseService;

public class RoomCourseServiceImpl implements RoomCourseService {

	RoomCourseDao roomCourseDao = new RoomCourseDaoImpl();
	@Override
	public List<RoomCourse> queryTimeByCourse() {
		// TODO Auto-generated method stub
		return roomCourseDao.queryTimeByCourse();
	}
	@Override
	public int addRoomCourseByRoomCourse(RoomCourse roomCourse) {
		// TODO Auto-generated method stub
		return roomCourseDao.addRoomCourseByRoomCourse(roomCourse);
	}
	@Override
	public int deleteRoomCourseByRoomCourseId(int roomCourseId) {
		// TODO Auto-generated method stub
		return roomCourseDao.deleteRoomCourseByRoomCourseId(roomCourseId);
	}

}
