package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.PermissionDao;
import cn.edu.sanxiau.www.dao.impl.PermissionDaoImpl;
import cn.edu.sanxiau.www.model.Permission;
import cn.edu.sanxiau.www.service.PermissionService;

public class PermissionServiceImpl implements PermissionService {
	PermissionDao permissionDao = new PermissionDaoImpl();

	@Override
	public List<Permission> queryAllPermission() {
		return permissionDao.queryAllPermission();
	}

	@Override
	public int addPermissionByPermission(Permission permission) {
		return permissionDao.addPermissionByPermission(permission);
	}

	@Override
	public int deletePermissionByPermissionId(int permissionId) {
		return permissionDao.deletePermissionByPermissionId(permissionId);
	}

	@Override
	public Permission queryPermissionByPermissionId(int permissionId) {
		return permissionDao.queryPermissionByPermissionId(permissionId);
	}

	@Override
	public Permission queryPermissionBypId(int pId) {
		return permissionDao.queryPermissionBypId(pId);
	}

	@Override
	public int updatePermissionByPermission(Permission permission) {
		return permissionDao.updatePermissionByPermission(permission);
	}

	@Override
	public List<Permission> queryPermissionByroleId(int roleId) {
		return permissionDao.queryPermissionByroleId(roleId);
	}

	@Override
	public List<Permission> queryAllPermissionByUserId(int userId) {
		return permissionDao.queryAllPermissionByUserId(userId);
	}

	@Override
	public List<Permission> queryUser_xz_permissionByUserId(int userId) {
		return permissionDao.queryUser_xz_permissionByUserId(userId);
	}

	@Override
	public List<Permission> queryXZPermissionAllSonByPId(int pId) {
		return permissionDao.queryXZPermissionAllSonByPId(pId);
	}

}
