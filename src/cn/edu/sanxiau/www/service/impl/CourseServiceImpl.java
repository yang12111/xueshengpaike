package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.CourseDao;
import cn.edu.sanxiau.www.dao.impl.CourseDaoImpl;
import cn.edu.sanxiau.www.model.Course;
import cn.edu.sanxiau.www.model.PaiKe;
import cn.edu.sanxiau.www.model.User2;
import cn.edu.sanxiau.www.service.CourseService;
import frame.utils.model.PageBean;

public class CourseServiceImpl implements CourseService {
	CourseDao courseDao = new CourseDaoImpl();
	public List<Course> queryAllCourse() {
		return courseDao.queryAllCourse();
	}
	@Override
	public int addCourseByCourse(Course course) {
		return courseDao.addCourseByCourse(course);
	}
	@Override
	public int deleteCourseByCourseId(int courseId) {
		return courseDao.deleteCourseByCourseId(courseId);
	}
	@Override
	public Course queryCourseByCourseId(int courseId) {
		return courseDao.queryCourseByCourseId(courseId);
	}
	@Override
	public int updateCourseByCourse(Course course) {
		return courseDao.updateCourseByCourse(course);
	}
	
	@Override
	public PageBean<Course> queryAllPage(int pc, int ps) {
		PageBean<Course> pb = new PageBean<Course>();
		pb.setPc(pc);
		pb.setPs(ps);

		// 查询总记录数:总记录数（total record）
		int tr = courseDao.queryUser2TotalRecord();
		pb.setTr(tr);

		// 查询当前页的记录
		List<Course> beanList = courseDao.queryCurrentPageDataList((pc - 1) * ps, ps);
		pb.setBeanList(beanList);
		return pb;
	}
	@Override
	public List<Course> queryAllCourseByState(String state) {
		// TODO Auto-generated method stub
		return courseDao.queryAllCourseByState(state);
	}

	@Override
	public List<PaiKe> queryPkBycourseId() {
		// TODO Auto-generated method stub
		return courseDao.queryPkBycourseId();
	}

	@Override
	public int updateCourseStateByCourseId(int courseId) {
		// TODO Auto-generated method stub
		return courseDao.updateCourseStateByCourseId(courseId);
	}

	@Override
	public List<PaiKe> queryPkBycourseId(int courseId) {
		// TODO Auto-generated method stub
		return courseDao.queryPkBycourseId(courseId);
	}

	@Override
	public int updateCourseByPaiKe(PaiKe paike) {
		// TODO Auto-generated method stub
		return courseDao.updateCourseByPaiKe(paike);
	}

}
