package cn.edu.sanxiau.www.service.impl;

import java.util.List;

import cn.edu.sanxiau.www.dao.PageDao;
import cn.edu.sanxiau.www.dao.impl.PageDaoImpl;
import cn.edu.sanxiau.www.model.PaiKe;
import cn.edu.sanxiau.www.model.User2;
import cn.edu.sanxiau.www.service.PageService;
import frame.utils.model.PageBean;

public class PageServiceImpl implements PageService {
	PageDao pageDao = new PageDaoImpl();

	@Override
	public PageBean<PaiKe> queryAllPage(int pc, int ps) {
		// 实例化PageBean
				PageBean<PaiKe> pb = new PageBean<PaiKe>();
				pb.setPc(pc);
				pb.setPs(ps);

				// 查询总记录数:总记录数（total record）
				int tr = pageDao.queryUser2TotalRecord();
				pb.setTr(tr);

				// 查询当前页的记录
				List<PaiKe> beanList = pageDao.queryCurrentPageDataList((pc - 1) * ps, ps);
				pb.setBeanList(beanList);
				return pb;
	}

}
