package cn.edu.sanxiau.www.service;

import java.util.List;

import cn.edu.sanxiau.www.model.User;

import frame.utils.model.PageBean;

public interface UserService {

	User queryUserByUser(User user);

	List<User> queryAllUser();

	int addUserByUser(User user);

	int deleteUserByUserId(int userId);

	User queryUserByUserId(int userId);

	int updateUserByUser(User user);

	

	
}
