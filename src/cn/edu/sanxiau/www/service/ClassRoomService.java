package cn.edu.sanxiau.www.service;

import java.util.List;

import cn.edu.sanxiau.www.model.ClassRoom;
import cn.edu.sanxiau.www.model.PaiKe;
import cn.edu.sanxiau.www.model.PaiRoom;
import cn.edu.sanxiau.www.model.RoomCourse;
import frame.utils.model.PageBean;

public interface ClassRoomService {

	List<ClassRoom> queryAllClassroom();

	int addClassRoomByClassRoomr(ClassRoom classroom);

	int deleteClassRoomByClassRoomId(int roomId);

	ClassRoom queryClassRoomByClassRoomId(int roomId);

	int updateClassRoomByClassRoom(ClassRoom classroom);

	PageBean<ClassRoom> queryAllPage(int pc, int ps);

	List<ClassRoom> queryClassRoomByCourseId(int courseId);

	String queryRoomNameByCourseId(int courseId);

	int updateRoomStateByRoomId(int roomId);
	
	List<PaiKe> queryAllCourse();           ///addUI

	List<PaiKe> queryAllRoomByRoomId(int roomId);     //queryEmputyRoom

	List<PaiKe> queryAllClassRoom();
	
	List<PaiRoom> queryAllClassRoom2();        //queryEmputyRoom

	List<PaiRoom> queryAllExam();     ///addUI

	List<PaiRoom> queryAllRoomByRoomId2(int roomId);   //queryEmputyRoom


}
