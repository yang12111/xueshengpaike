package cn.edu.sanxiau.www.service;

import java.util.List;

import cn.edu.sanxiau.www.model.Permission;

public interface PermissionService {

	List<Permission> queryAllPermission();

	int addPermissionByPermission(Permission permission);

	int deletePermissionByPermissionId(int permissionId);

	Permission queryPermissionByPermissionId(int permissionId);

	Permission queryPermissionBypId(int pId);

	int updatePermissionByPermission(Permission permission);

	List<Permission> queryPermissionByroleId(int roleId);

	List<Permission> queryAllPermissionByUserId(int userId);

	List<Permission> queryUser_xz_permissionByUserId(int userId);

	List<Permission> queryXZPermissionAllSonByPId(int pId);

}
