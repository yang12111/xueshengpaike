package cn.edu.sanxiau.www.service;

import cn.edu.sanxiau.www.model.PolePermission;

public interface PolePermissionService {

	int addPolePermissionByPolePermission(PolePermission polePermission);

	int deletePolePermissionByRoleId(int roleId);

}
