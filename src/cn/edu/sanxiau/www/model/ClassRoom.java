package cn.edu.sanxiau.www.model;

public class ClassRoom {
	
	private int roomId;
	
	private String roomName;
	
	private String capacity;
	
	private String state;
	
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "ClassRoom [roomId=" + roomId + ", roomName=" + roomName + ", capacity=" + capacity + ", state=" + state
				+ "]";
	}

}
