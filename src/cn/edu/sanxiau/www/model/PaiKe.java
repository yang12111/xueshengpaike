package cn.edu.sanxiau.www.model;

public class PaiKe {
	private int courseId;
	private String courseName;
	private String courseTeacher;
	private String score;
	private String state;
	
	private int roomId;
	private String roomName;
	private String capacity;
	
	private int roomCourseId;
	private String time;
	private String date;
	private int preroomId;
	private String rdate;
	
	public String getRdate() {
		return rdate;
	}
	public void setRdate(String rdate) {
		this.rdate = rdate;
	}
	public int getPreroomId() {
		return preroomId;
	}
	public void setPreroomId(int preroomId) {
		this.preroomId = preroomId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getCourseId() {
		return courseId;
	}
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseTeacher() {
		return courseTeacher;
	}
	public void setCourseTeacher(String courseTeacher) {
		this.courseTeacher = courseTeacher;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public int getRoomCourseId() {
		return roomCourseId;
	}
	public void setRoomCourseId(int roomCourseId) {
		this.roomCourseId = roomCourseId;
	}

}
