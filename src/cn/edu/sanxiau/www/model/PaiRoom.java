package cn.edu.sanxiau.www.model;

public class PaiRoom {
	private int examId;
	private String examName;
	private String examType;
	private String examState;
	
	private int roomId;
	private String roomName;
	private String capacity;
	
	private int roomExamId;
	private String time;
	private String date;
	
	private int preroomId;
	private String rdate;
	public int getExamId() {
		return examId;
	}
	public void setExamId(int examId) {
		this.examId = examId;
	}
	public String getExamName() {
		return examName;
	}
	public void setExamName(String examName) {
		this.examName = examName;
	}
	public String getExamType() {
		return examType;
	}
	public void setExamType(String examType) {
		this.examType = examType;
	}
	public String getExamState() {
		return examState;
	}
	public void setExamState(String examState) {
		this.examState = examState;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	public int getRoomExamId() {
		return roomExamId;
	}
	public void setRoomExamId(int roomExamId) {
		this.roomExamId = roomExamId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getPreroomId() {
		return preroomId;
	}
	public void setPreroomId(int preroomId) {
		this.preroomId = preroomId;
	}
	public String getRdate() {
		return rdate;
	}
	public void setRdate(String rdate) {
		this.rdate = rdate;
	}
	
	
}
