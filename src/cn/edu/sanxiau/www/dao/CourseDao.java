package cn.edu.sanxiau.www.dao;

import java.util.List;

import cn.edu.sanxiau.www.model.Course;
import cn.edu.sanxiau.www.model.PaiKe;

public interface CourseDao {

	List<Course> queryAllCourse();

	int addCourseByCourse(Course course);

	int deleteCourseByCourseId(int courseId);

	Course queryCourseByCourseId(int courseId);

	int updateCourseByCourse(Course course);

	int queryUser2TotalRecord();

	List<Course> queryCurrentPageDataList(int i, int ps);
	List<Course> queryAllCourseByState(String state);

	List<PaiKe> queryPkBycourseId();

	int updateCourseStateByCourseId(int courseId);

	List<PaiKe> queryPkBycourseId(int courseId);

	int updateCourseByPaiKe(PaiKe paike);

}
