package cn.edu.sanxiau.www.dao;

import java.util.List;

import cn.edu.sanxiau.www.model.Exam;
import cn.edu.sanxiau.www.model.PaiRoom;

public interface ExamDao {

	 

	List<Exam> queryAllExam();

	 
	int addExamByExam(Exam exam);


	int deleteExamByExamId(int examId);


	Exam queryExamByExamId(int examId);


	int updateExamByExam(Exam exam);


	int queryUser2TotalRecord();


	List<Exam> queryCurrentPageDataList(int i, int ps);
	
	


	List<PaiRoom> queryPrByExamId();   //list


	List<Exam> queryAllExamByState(String string);         ////addUI2

	 

	 

	 
	 

	 

}
