package cn.edu.sanxiau.www.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import cn.edu.sanxiau.www.dao.PageDao;
import cn.edu.sanxiau.www.model.PaiKe;
import cn.edu.sanxiau.www.model.User2;
import frame.utils.dbutil.JdbcUtils;

public class PageDaoImpl implements PageDao {

	@Override
	public int queryUser2TotalRecord() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = JdbcUtils.getConnection();
			String sql = "SELECT count(*) FROM  room_course";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				int count = rs.getInt(1);
				return count;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<PaiKe> queryCurrentPageDataList(int i, int ps) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = JdbcUtils.getConnection();
			String sql = "SELECT room_course.date, room_course.room_course_Id, classroom.roomId, classroom.roomName, room_course.time, course.courseId, course.courseName, course.courseTeacher, course.score, course.state FROM (classroom , course) INNER JOIN room_course ON course.courseId = room_course.courseId AND room_course.roomId = classroom.roomId LIMIT ?,?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, i);
			pstmt.setInt(2, ps);
			rs = pstmt.executeQuery();
			List<PaiKe> paike_db_list = new ArrayList<PaiKe>();

			while (rs.next()) {
				PaiKe paike_db = new PaiKe();
				paike_db.setDate(rs.getString("room_course.date"));
				paike_db.setRoomCourseId(rs.getInt("room_course.room_course_Id"));
				paike_db.setRoomId(rs.getInt("classroom.roomId"));
				paike_db.setTime(rs.getString("room_course.time"));
				paike_db.setRoomName(rs.getString("classroom.roomName"));
				paike_db.setCourseId(rs.getInt("course.courseId"));
				paike_db.setCourseName(rs.getString("course.courseName"));
				paike_db.setCourseTeacher(rs.getString("course.courseTeacher"));
				paike_db.setScore(rs.getString("course.score"));
				paike_db.setState(rs.getString("course.state"));
				paike_db_list.add(paike_db);
			}
			return paike_db_list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
