package cn.edu.sanxiau.www.dao.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cn.edu.sanxiau.www.dao.RoleDao;
import cn.edu.sanxiau.www.model.Role;
import frame.utils.dbutil.JdbcUtils;

public class RoleDaoImpl implements RoleDao {

	@Override
	public List<Role> queryAllRole() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		// 查询数据库
		try {
			conn = JdbcUtils.getConnection();
			String sql = "SELECT * FROM sys_role";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();

			List<Role> role_db_list = new ArrayList<Role>();

			while (rs.next()) {
				Role role_db = new Role();
				role_db.setRoleId(rs.getInt("roleId"));
				role_db.setName(rs.getString("name"));
				role_db.setState(rs.getString("state"));

				role_db_list.add(role_db);
			}

			return role_db_list;

		} catch (ClassNotFoundException | IOException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int addRoleByRole(Role role) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = JdbcUtils.getConnection();
			String sql = "INSERT INTO  sys_role (roleId,name,state) VALUES(?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, role.getRoleId());
			pstmt.setString(2, role.getName());
			pstmt.setString(3, role.getState());

			int m = pstmt.executeUpdate();
			return m;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	@Override
	public int deleteRoleByRoleId(int roleId) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = JdbcUtils.getConnection();

			String sql = "DELETE FROM sys_role WHERE roleId=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, roleId);

			int m = pstmt.executeUpdate();
			return m;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Role queryRoleByRoleId(int roleId) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		// 查询数据库
		try {
			conn = JdbcUtils.getConnection();
			String sql = "SELECT * FROM sys_role WHERE roleId=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, roleId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Role role_db = new Role();
				role_db.setRoleId(rs.getInt("roleId"));
				role_db.setName(rs.getString("name"));
				role_db.setState(rs.getString("state"));
				return role_db;
			}
		} catch (ClassNotFoundException | IOException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int updateRoleByRoler(Role role) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = JdbcUtils.getConnection();
			String sql = "UPDATE sys_role SET name=?,state=? WHERE roleId=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, role.getName());
			pstmt.setString(2, role.getState());
			pstmt.setInt(3, role.getRoleId());

			int m = pstmt.executeUpdate();
			return m;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Role queryRoleByUserId(int userId) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		// 查询数据库
		try {
			conn = JdbcUtils.getConnection();
			String sql = "SELECT sys_role.roleId,sys_role.`name`,sys_role.state FROM sys_user_role INNER JOIN sys_role ON sys_user_role.roleId = sys_role.roleId WHERE userId=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userId);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Role role_db = new Role();
				role_db.setRoleId(rs.getInt("roleId"));
				role_db.setName(rs.getString("name"));
				role_db.setState(rs.getString("state"));
				return role_db;
			}
		} catch (ClassNotFoundException | IOException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
