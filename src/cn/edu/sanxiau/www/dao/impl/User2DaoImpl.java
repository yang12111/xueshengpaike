package cn.edu.sanxiau.www.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import cn.edu.sanxiau.www.dao.User2Dao;
import cn.edu.sanxiau.www.model.User2;
import frame.utils.dbutil.JdbcUtils;

public class User2DaoImpl implements User2Dao {

	@Override
	public int queryUser2TotalRecord() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = JdbcUtils.getConnection();
			String sql = "SELECT count(*) FROM  sys_user2";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				int count = rs.getInt(1);
				return count;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<User2> queryCurrentPageDataList(int i, int ps) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = JdbcUtils.getConnection();
			String sql = "SELECT * FROM  sys_user2 LIMIT ?,?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, i);
			pstmt.setInt(2, ps);
			rs = pstmt.executeQuery();
			List<User2> user2List = new ArrayList<>();
			while (rs.next()) {
				User2 user2_db = new User2();
				user2_db.setUserId(rs.getInt("userId"));
				user2_db.setName(rs.getString("name"));
				user2_db.setSex(rs.getString("sex"));
				user2_db.setAdress(rs.getString("adress"));
				user2_db.setTel(rs.getString("tel"));
				user2_db.setUserName(rs.getString("userName"));
				user2_db.setPassword(rs.getString("password"));
				user2List.add(user2_db);
			}
			return user2List;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
