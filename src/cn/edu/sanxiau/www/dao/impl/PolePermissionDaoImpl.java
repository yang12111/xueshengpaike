package cn.edu.sanxiau.www.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import cn.edu.sanxiau.www.dao.PolePermissionDao;
import cn.edu.sanxiau.www.model.PolePermission;
import frame.utils.dbutil.JdbcUtils;

public class PolePermissionDaoImpl implements PolePermissionDao {

	@Override
	public int addPolePermissionByPolePermission(PolePermission polePermission) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = JdbcUtils.getConnection();
			String sql = "INSERT INTO  sys_role_permission (rolePermissionId,roleId,permissionId) VALUES(?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, polePermission.getRolePermissionId());
			pstmt.setInt(2, polePermission.getRoleId());
			pstmt.setInt(3, polePermission.getPermissionId());
			int m = pstmt.executeUpdate();
			return m;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	@Override
	public int deletePolePermissionByRoleId(int roleId) {
		Connection conn = null;
		PreparedStatement pstmt = null;

		try {
			conn = JdbcUtils.getConnection();

			String sql = "DELETE FROM sys_role_permission WHERE roleId=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, roleId);

			int m = pstmt.executeUpdate();
			return m;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
