package cn.edu.sanxiau.www.dao;

import java.util.List;


import cn.edu.sanxiau.www.model.ClassRoom;
import cn.edu.sanxiau.www.model.PaiKe;
import cn.edu.sanxiau.www.model.PaiRoom;
import cn.edu.sanxiau.www.model.RoomCourse;

public interface ClassRoomDao {

	List<ClassRoom> queryAllClassroom();

	int addClassRoomByClassRoomr(ClassRoom classroom);

	int deleteClassRoomByClassRoomId(int roomId);

	ClassRoom queryClassRoomByClassRoomId(int roomId);

	int updateClassRoomByClassRoom(ClassRoom classroom);

	int queryClassRoomTotalRecord();

	List<ClassRoom> queryCurrentPageDataList(int i, int ps);
	List<PaiKe> queryAllCourse();          ///addUI

	List<ClassRoom> queryClassRoomByCourseId(int courseId);

	String queryRoomNameByCourseId(int courseId);

	int updateRoomStateByRoomId(int roomId);

	List<PaiKe> queryAllRoomByRoomId(int roomId);

	List<PaiKe> queryAllClassRoom();

	List<PaiRoom> queryAllExam();         //dddUI

	List<PaiRoom> queryAllClassRoom2();     //queryAllRoomByRoomId2(int roomId)

	List<PaiRoom> queryAllRoomByRoomId2(int roomId);     //queryAllRoomByRoomId2(int roomId)


}
