package cn.edu.sanxiau.www.dao;

import cn.edu.sanxiau.www.model.UserRole;

public interface UserRoleDao {

	int addUserRoleByUserRole(UserRole userRole);

	int deleteUserRoleByUserId(int userId);

}
