package cn.edu.sanxiau.www.dao;

import java.util.List;

import cn.edu.sanxiau.www.model.RoomCourse;
import cn.edu.sanxiau.www.model.RoomExam;

public interface RoomCourseDao {

	List<RoomCourse> queryTimeByCourse();

	int addRoomCourseByRoomCourse(RoomCourse roomCourse);

	int deleteRoomCourseByRoomCourseId(int roomCourseId);

	int addRoomExamByRoomExam(RoomExam roomExam);    //add

}
