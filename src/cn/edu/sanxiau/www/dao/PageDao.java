package cn.edu.sanxiau.www.dao;

import java.util.List;

import cn.edu.sanxiau.www.model.PaiKe;

public interface PageDao {

	int queryUser2TotalRecord();

	List<PaiKe> queryCurrentPageDataList(int i, int ps);

}
