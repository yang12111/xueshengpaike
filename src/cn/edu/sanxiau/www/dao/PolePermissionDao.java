package cn.edu.sanxiau.www.dao;

import cn.edu.sanxiau.www.model.PolePermission;

public interface PolePermissionDao {

	int addPolePermissionByPolePermission(PolePermission polePermission);

	int deletePolePermissionByRoleId(int roleId);

}
