<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>网站后台管理系统HTML模板--模板之家 www.cssmoban.com</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  $(".click").click(function(){
  $(".tip").fadeIn(200);
  });
  
  $(".tiptop a").click(function(){
  $(".tip").fadeOut(200);
});

  $(".sure").click(function(){
  $(".tip").fadeOut(100);
});

  $(".cancel").click(function(){
  $(".tip").fadeOut(100);
});

});
</script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">数据表</a></li>
    <li><a href="#">基本内容</a></li>
    </ul>
    </div>
    
    <div class="rightinfo">
    
    <div class="tools">
    
    	<ul class="toolbar">
        <li ><span><img src="images/t01.png" /></span><a href="<%=basePath%>view/system/classroom/addClassRoom.jsp">增加</a></li>
        <li class="click"><span><img src="images/t02.png" /></span>修改</li>
        <li><span><img src="images/t03.png" /></span>删除</li>
        <li><span><img src="images/t04.png" /></span>统计</li>
        </ul>
        
        
        <ul class="toolbar1">
        <li><span><img src="images/t05.png" /></span>设置</li>
        </ul>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr>
        <th><input name="" type="checkbox" value="" checked="checked"/></th>
        <th>教室ID<i class="sort"><img src="images/px.gif" /></i></th>
        <th>教室名称</th>
        <th>教室容量</th>
        <th>状态</th>
        <th>操作</th>
        </tr>
        </thead>
        
        <tbody>
       <%--  <c:forEach items="${userPage_db_list }" var="userPage_db"> --%>
       <c:forEach items="${pb.beanList }" var="classroom">
        <tr>
        <td><input name="" type="checkbox" value="" /></td>
        <td>${classroom.roomId }</td>
        <td>${classroom.roomName} </td>
        <td>${classroom.capacity }</td>
        <c:if test="${classroom.state != 0}">
        <td>有课</td>
        </c:if>
        <c:if test="${classroom.state == 0}">
        <td>空</td>
        </c:if>
        <td><a href="<%=basePath%>classRoomControl.do?method=delete&roomIdStr=${classroom.roomId }" class="tablelink"> 删除</a>
         <a href="<%=basePath%>classRoomControl.do?method=updateUI&roomIdStr=${classroom.roomId }" class="tablelink"> 修改</a>
        </tr> 
        </c:forEach> 
        </tbody>
        				<tr>
					<td colspan="8"><div class="pagelist">
							第${pb.pc }页/共${pb.tp }页 <a
								href="<c:url value='/classRoomControl.do?method=list&pc=1'/>">首页</a>
							<c:if test="${pb.pc>1 }">
								<a
									href="<c:url value='/classRoomControl.do?method=list&pc=${pb.pc-1 }'/>">上一页</a>
							</c:if>
							<%-- （2）计算：begin、end --%>
							<c:choose>
								<%-- 如果总页数不足10页，那么把所有的页数都显示出来！ --%>
								<c:when test="${pb.tp <= 10 }">
									<c:set var="begin" value="1" />
									<c:set var="end" value="${pb.tp }" />
								</c:when>
								<c:otherwise>
									<%--当总页数>10时，通过公式计算出begin和end --%>
									<c:set var="begin" value="${pb.pc-5}" />
									<c:set var="end" value="${pb.pc+4 }" />
									<%--头溢出 --%>
									<c:if test="${begin < 1 }">
										<c:set var="begin" value="1" />
										<c:set var="end" value="10" />
									</c:if>
									<%--尾溢出 --%>
									<c:if test="${end > pb.tp }">
										<c:set var="begin" value="${pb.tp-9}" />
										<c:set var="end" value="${pb.tp}" />
									</c:if>
								</c:otherwise>
							</c:choose>
							<%-- （3）循环遍历页码列表 --%>
							<c:forEach var="i" begin="${begin }" end="${end }">
								<c:choose>
									<c:when test="${i eq pb.pc}">
										<a style="color: green; font-weight: bold; font-size: 16px;"
											href="<c:url value='/classRoomControl.do?method=list&pc=${i }'/>">${i }</a>
									</c:when>
									<c:otherwise>
										<a
											href="<c:url value='/classRoomControl.do?method=list&pc=${i }'/>">${i }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<c:if test="${pb.pc<pb.tp }">
								<a
									href="<c:url value='/classRoomControl.do?method=list&pc=${pb.pc+1 }'/>">下一页</a>
							</c:if>
							<a
								href="<c:url value='/classRoomControl.do?method=list&pc=${pb.tp }'/>">尾页</a>
						</div></td>
				</tr>
    </table>
    
   
<!--     <div class="pagin">
    	<div class="message">共<i class="blue">1256</i>条记录，当前显示第&nbsp;<i class="blue">2&nbsp;</i>页</div>
        <ul class="paginList">
        <li class="paginItem"><a href="javascript:;"><span class="pagepre"></span></a></li>
        <li class="paginItem"><a href="javascript:;">1</a></li>
        <li class="paginItem current"><a href="javascript:;">2</a></li>
        <li class="paginItem"><a href="javascript:;">3</a></li>
        <li class="paginItem"><a href="javascript:;">4</a></li>
        <li class="paginItem"><a href="javascript:;">5</a></li>
        <li class="paginItem more"><a href="javascript:;">...</a></li>
        <li class="paginItem"><a href="javascript:;">10</a></li>
        <li class="paginItem"><a href="javascript:;"><span class="pagenxt"></span></a></li>
        </ul>
    </div> -->
    
    
    <div class="tip">
    	<div class="tiptop"><span>提示信息</span><a></a></div>
        
      <div class="tipinfo">
        <span><img src="images/ticon.png" /></span>
        <div class="tipright">
        <p>是否确认对信息的修改 ？</p>
        <cite>如果是请点击确定按钮 ，否则请点取消。</cite>
        </div>
        </div>
        
        <div class="tipbtn">
        <input name="" type="button"  class="sure" value="确定" />&nbsp;
        <input name="" type="button"  class="cancel" value="取消" />
        </div>
    
    </div>
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>
</html>
