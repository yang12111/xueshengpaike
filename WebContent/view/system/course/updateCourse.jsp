<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">表单</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
		<form action="<%=basePath%>courseControl.do?method=update" method="post">
		<input type="hidden" name="courseIdStr" value="${course_db.courseId }">
    <ul class="forminfo">
    <li><label>课程名称</label><input name="courseName" type="text" class="dfinput" value="${course_db.courseName }"/><i>性别不能超过30个字符</i></li>
     <li><label>任课老师</label><input name="courseTeacher" type="text" class="dfinput" value="${course_db.courseTeacher }"/><i>姓名不能超过30个字符</i></li>
    <li><label>学分</label><input name="score" type="text" class="dfinput" value="${course_db.score }"/><i>性别不能超过30个字符</i></li>
  <li><label>&nbsp;</label><input name="" type="submit" class="btn" value="确认保存"/></li> 
    </ul>
    </form>
    
    </div>
</body>
</html>
