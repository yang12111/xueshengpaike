<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<h1>用户列表页面</h1>
	</center>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="<%=basePath%>view/system/user/addUser.jsp">增加</a>
	<center>
		<hr>
		<table border="1" width="80%">
			<tr>
				<td>用户ID</td>
				<td>姓名</td>
				<td>性别</td>
				<td>地址</td>
				<td>电话</td>
				<td>登录名</td>
				<td>密码</td>
				<td colspan="2">操作</td>
			</tr>
			<c:forEach items="${user_db_list }" var="user_db">
			<tr>
				<td>${user_db.userId }</td>
				<td>${user_db.name }</td>
				<td>${user_db.sex }</td>
				<td>${user_db.adress }</td>
				<td>${user_db.tel }</td>
				<td>${user_db.userName }</td>
				<td>${user_db.password }</td>
				<td><a href="<%=basePath%>userControl.do?method=delete&userIdStr=${user_db.userId }">删除</a></td>
				<td><a href="<%=basePath%>userControl.do?method=updateUI&userIdStr=${user_db.userId }">修改</a></td>
			</tr>
		</c:forEach>
		</table>
		<br> <input name="" type="button" value="上一页"><input
			name="" type="button" value="1"><input name="" type="button"
			value="2"><input name="" type="button" value="3"><input
			name="" type="button" value="4"><input name="" type="button"
			value="5"><input name="" type="button" value="6"><input
			name="" type="button" value="7"><input name="" type="button"
			value="8"><input name="" type="button" value="9"><input
			name="" type="button" value="下一页">
	</center>
</body>
</html>