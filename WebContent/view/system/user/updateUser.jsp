<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">表单</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
		<form action="<%=basePath%>userControl.do?method=update" method="post">
		<input type="hidden" name="userIdStr" value="${user_db.userId }">
    <ul class="forminfo">
    <li><label>姓名</label><input name="name" type="text" class="dfinput" value="${user_db.name }"/><i>姓名不能超过30个字符</i></li>
    <li><label>性别</label><input name="sex" type="text" class="dfinput" value="${user_db.sex }"/><i>性别不能超过30个字符</i></li>
    <li><label>地址</label><input name="adress" type="text" class="dfinput" value="${user_db.adress }"/><i>地址不能超过30个字符</i></li>
    <li><label>电话</label><input name="tel" type="text" class="dfinput"  value="${user_db.tel }"/><i>电话不能超过30个字符</i></li>
    <li><label>登录名</label><input name="userName" type="text" class="dfinput" value="${user_db.userName }"/><i>登录名不能超过30个字符</i></li>
    <li><label>密码</label><input name="password" type="text" class="dfinput" value="${user_db.password }"/><i>密码不能超过30个字符</i></li>
    <li><label>&nbsp;</label><input name="" type="submit" class="btn" value="确认保存"/></li> 
    </ul>
    </form>
    
    </div>
</body>
</html>
