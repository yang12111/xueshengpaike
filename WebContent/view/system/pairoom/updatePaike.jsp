<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">表单</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>排课信息</span></div>
		<form action="<%=basePath%>paikeControl.do?method=queryEmputyRoom1" method="post">
		<c:forEach items="${paike_db_list }" var="paike_db_list">
		<input type="hidden" name="courseIdStr" value="${paike_db_list.courseId }">
		<input type="hidden" name="roomCourseIdStr" value="${paike_db_list.roomCourseId }">
		<input type="hidden" name="preroomIdStr" value="${paike_db_list.roomId }">
    <ul class="forminfo">
    <li><label>课程名称</label>${paike_db_list.courseName }</li>
    <li><label>任课教师：</label>${paike_db_list.courseTeacher }</li>
    <li><label>星期</label>
		<select id="date" name="date">
					<option value="${paike_db_list.date}">${paike_db_list.date}</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
			</select></li>
    <li><label>时间</label>
		<select id="time" name="time">
					<option value="${paike_db_list.time}">${paike_db_list.time}</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
			</select></li>
    <li><label>&nbsp;</label><input name="" type="submit" class="btn" value="查询空教室"/></li> 
    </ul>
    </c:forEach>
    </form>
    
    </div>
</body>
</html>